#!/bin/bash
# MIT License
# Copyright (c) 2017 Ken Fallon http://kenfallon.com
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# Fix to ignore more networks.

##################################################
# Editing below this line should not be necessary



if [ $( which arp-scan 2>/dev/null | wc -l ) -eq 1 ]
then
  echo "Has arp-scan ..."
  ifconfig | awk -F ':' '/: flags/ {print $1}' | grep -vE 'lo|loop|docker|ppp|vmnet' | while read interface
  do
    arp-scan --quiet --interface ${interface} --localnet --numeric --ignoredups 2>/dev/null | grep -E "([0-9]{1,3}[\.]){3}[0-9]{1,3}.*([0-9A-Fa-f]{2}[:]){5}[0-9A-Fa-f]{2}"
  done

fi | grep -iE 'b8:27:eb|dc:a6:32' | sort | while read ip mac

do


  if [ -z "${host_name}" ]
  then
    host_name="$( echo ${mac})"
  fi

  echo "$(echo ${mac})=${ip}"

done
