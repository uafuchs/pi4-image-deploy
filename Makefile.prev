# DISTRO=raspios_arm64_lite-2020-08-20

NFS_ROOT=192.168.178.10
CONTROLLER_SN=9a34f668
CLIENT_SN=d11d4ea4



url_base="https://downloads.raspberrypi.org/raspios_lite_arm64/images"

version="$(shell  wget -q ${url_base} -O - | xmllint --html --xmlout --xpath 'string(/html/body/table/tr[last()-1]/td/a/@href)' - )"
sha_file=$(shell wget -q ${url_base}/${version} -O - | xmllint --html --xmlout --xpath 'string(/html/body/table/tr/td/a[contains(@href, "256")])' - )
sha_sum=$(shell wget -q "${url_base}/${version}/${sha_file}" -O - | awk '{print $1}' )

# YAML to JSON

YAMLLINT=yamllint
YAMLLINT_RULES="{extends: default, rules: {line-length: {max: 140}}}"

# check for yamllint
ifeq ($(shell which $(YAMLLINT) >/dev/null 2>&1; echo $$?), 1)
	$(error The '$(YAMLLINT)' command was not found. Please install '$(YAMLLINT)' )
else
    $(echo Test)
endif

# yaml2json
# @param1 string - base file name of the template
define yaml2json
	$(YAMLLINT) -d $(YAMLLINT_RULES) $(1).yml
	ruby -rjson -ryaml -e "puts JSON.pretty_generate(YAML.load_file('$(1).yml'))" > $(1).json
	rm *.yml
endef


GIGA_BYTE=1073741824

export DISTRO=raspios
export VERSION=lite_arm64
export RELEASE=2020-08-20
export PUBLISH=2020-08-24
export DRV_DIR=$(DISTRO)/$(VERSION)/$(RELEASE)
TEMPLATE_DIR=templates/$(DRV_DIR)

###############################################################################
# base image
###############################################################################

# adjust_cluster_base_image
# @param1 string - directory of packer template file, eg. 'templates/raspios/arm64_lite/2020-08-20'
# @param2 number - {{image_size}}, size of the image in giga bytes, eg. 3
# @param3 array  - provisioners
define adjust_cluster_base_image
	cat $(1)/$@.yml | \
		sed 's/{{image_size}}/$(shell echo ${GIGA_BYTE}*$(2) | bc)/g' > $@.yml
	cat $@.yml \
		$(foreach word,$(3),provisioners/$(word).yml) \
		post-processors/sha256sum.yml > packer-$@.yml
endef

PHONY: prepare-cluster-base-image
prepare-cluster-base-image:
	$(MAKE) -C templates prepare-cluster-base-image

.PHONY: cluster-base-image
cluster-base-image:	private IMAGE_SIZE=2 # in GB
cluster-base-image: private PROVISIONERS= \
	enable-serial0 \
	disable-sound \
	disable-bluez+wlan \
	install-bcmstat \
	raspi-config \
	ssh \
	system-prepare
cluster-base-image: prepare-cluster-base-image
	$(call adjust_cluster_base_image,$(TEMPLATE_DIR),$(IMAGE_SIZE),$(PROVISIONERS))
	$(call yaml2json,packer-$@)
	sudo packer build packer-$@.json
	sudo rm -rf out/$(DRV_DIR)
	sudo mkdir -p out/$(DRV_DIR)
	sudo mv $@.* out/$(DRV_DIR)/

###############################################################################
# custom images, inherites from base image
###############################################################################

PHONY: prepare-cluster-custom-image
prepare-cluster-custom-image:
	$(MAKE) -C templates prepare-cluster-custom-image

# adjust_cluster_custom_image
# @param1 string - name of distro, eg. 'raspios_arm64_lite-2020-08-20'
# @param2 number - {{image_size}}, size of the image in giga bytes, eg. 3
# @param3 number - provisioners
define adjust_cluster_custom_image
	cat $(1)/cluster-custom-image.yml | \
		sed 's/{{image_name}}/$@/g' | \
		sed 's/{{image_size}}/$(shell echo ${GIGA_BYTE}*$(2) | bc)/g' > $@.yml
	cat $@.yml \
		$(foreach word,$(3),provisioners/$(word).yml) > packer-$@.yml
endef

###
# cluster-controller
###

.PHONY: cluster-controller
cluster-controller: private IMAGE_SIZE=2 # in GB
cluster-controller: private PROVISIONERS= \
	install-controller-apps \
	enable-i2c \
	install-worker-power-supply \
	install-controller-fan
cluster-controller: prepare-cluster-custom-image
	$(call adjust_cluster_custom_image,$(TEMPLATE_DIR),$(IMAGE_SIZE),$(PROVISIONERS))
	$(call yaml2json,packer-$@)
	sudo packer build packer-$@.json
	sudo mv $@.* out/$(DRV_DIR)/


###
# cluster-worker
###

.PHONY: cluster-worker
cluster-worker:	private IMAGE_SIZE=2 # in GB
cluster-worker: private PROVISIONERS= \
	enable-i2c \
	install-fanhat-argon
cluster-worker: prepare-cluster-custom-image
	$(call adjust_cluster_custom_image,$(TEMPLATE_DIR),$(IMAGE_SIZE),$(PROVISIONERS))
	$(call yaml2json,packer-$@)
	sudo packer build packer-$@.json
	sudo mv $@.* out/$(DRV_DIR)/

.PHONY: clean
clean:
	@echo $(version)
	@echo $(sha_sum)
	#rm packer-*.json

.PHONY: dnsmasq
dnsmasq:
	sudo dnsmasq --no-daemon --log-dhcp --conf-file=/etc/dnsmasq.conf

.PHONY: arp-scan
arp-scan:
	sudo arp-scan --interface=enp0s31f6 --localnet
