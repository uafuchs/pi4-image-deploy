#!/bin/bash

# controller by mac address
CONTROLLER=dc:a6:32:6b:f0:42

# workers by mac address
WORKERS=( \
    dc:a6:32:77:7a:8d=1 \
    dc:a6:32:73:13:1a=2 \
    dc:a6:32:ba:7a:9c=3 \
    dc:a6:32:77:71:c9=4 \
)


SHUTDOWN_CMD="ssh -t pi@{{ip}} sudo shutdown -h now"
PING_CMD="ping -q -c 3 -W 3 {{ip}}"
# https://wiki.52pi.com/index.php/DockerPi_4_Channel_Relay_SKU:_EP-0099#Git_Repository
SWITCH_POWER_OFF_CMD="i2cset -y 1 0x10 0x0{{id}} 0xFF"

#
# isArpscanInstalled
#
isArpscanInstalled () {
    [ $( which arp-scan 2>/dev/null | wc -l ) -eq 1 ] && return 0 || return 1
}

#
# getRegisteredWorkers
#
getRegisteredWorkers () {

  ifconfig | awk -F ':' '/: flags/ {print $1}' | \
    grep -vE 'lo|loop|docker|ppp|vmnet' | \
    while read interface
    do
        arp-scan --quiet --interface ${interface} --localnet --numeric --ignoredups 2>/dev/null | grep -E "([0-9]{1,3}[\.]){3}[0-9]{1,3}.*([0-9A-Fa-f]{2}[:]){5}[0-9A-Fa-f]{2}"
    done | \
    grep -iE 'b8:27:eb|dc:a6:32' | sort | \
    while read ip mac
    do
        echo "$(echo ${mac})=${ip}"
    done

}

#
# Gets the key name of a key/value pair
# @param $1	the key/value pair
# qreturn	the key name of the key/value pair
getKey () {
  echo ${1%%=*}
}

#
# Gets the value of a key/value pair
# @param $1	the key/value pair
# qreturn	the value of the key/value pair
getValue () {
  echo "${1#*=}"
}

#

#
# @param $1	- IP address
#
shutDownWorker () {

    local ip="${1}"

    local cmd=`echo ${SHUTDOWN_CMD} | sed "s/{{ip}}/$ip/"`

    `$cmd`

    return $?

}

#
# @param $1	- IP address
#
waitForPingLoss () {

    local ip="${1}"

    local cmd=`echo ${PING_CMD} | sed "s/{{ip}}/$ip/"`

    local xy=`$cmd`

    return $?

}

#
# @param $1	- board ID
#
powerOff () {

    local id="${1}"

    local cmd=`echo ${SWITCH_POWER_OFF_CMD} | sed "s/{{id}}/$id/"`

    echo ${cmd}
}

#
# Gets the Raspberry Pi slot ID for a given MAC address
# @param $1	the MAC address
# qreturn	the slot ID
getBoardIDbyMAC () {

    for kv in ${WORKERS[@]}; do

        local mac=`getKey ${kv}`

        [[ "${1}" == "${mac}" ]] && {
            echo `getValue ${kv}`
            break
        }

    done

}


#
# main
#

if [ $(id | grep 'uid=0' | wc -l) -ne 1 ]
then
    echo "Error: You need to be root"
    exit 1
fi

isArpscanInstalled
[ "${?}" -eq 1 ] && {
    printf "Please install 'arp-scan'\n"
    exit 1
}

registeredPis="$(getRegisteredWorkers)"

[ "${#registeredPis}" -eq "0" ] && {
    printf "pi4-cluster is offline\n"
    exit 1
}

REGISTERED_WORKERS=($registeredPis)

[ ${#REGISTERED_WORKERS[@]} -eq 1 ] && {

    pi="${REGISTERED_WORKERS[0]}"

    mac=`getKey $pi`

    [[ "$mac" == "$CONTROLLER"  ]] && {
        printf "==> Only Controller on %s is still running.\n" `getValue $pi`
        exit 1
    }
}


for worker in ${REGISTERED_WORKERS[@]}
do

    echo $worker

    mac=`getKey $worker`

    [[ "$mac" == "$CONTROLLER"  ]] && {
        continue
    }

    ip=`getValue $worker`

    shutDownWorker "${ip}"
    [ "$?" -ne 0 ] && {
        printf "==> Failed to shutdown %s\n" "${ip}"
        continue
    }

    echo "waitForPingLoss"
    waitForPingLoss "${ip}"
    [ "$?" -ne 0 ] && {
        printf "==> Pingloss failed for %s\n" "${ip}"
        continue
    }

done











