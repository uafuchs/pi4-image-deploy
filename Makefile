# DISTRO=raspios_arm64_lite-2020-08-20

NFS_ROOT=192.168.178.10
CONTROLLER_SN=9a34f668
CLIENT_SN=d11d4ea4

# check for yamllint
ifeq ($(shell which $(YAMLLINT) >/dev/null 2>&1; echo $$?), 1)
	$(error The '$(YAMLLINT)' command was not found. Please install '$(YAMLLINT)' )
else
    $(echo Test)

.PHONY: cluster-base-image
cluster-base-image: prepare-cluster-base-image
	sudo rm -rf out/$(DRV_DIR)
	sudo mkdir -p out/$(DRV_DIR)
	sudo mv $@.* out/$(DRV_DIR)/

.PHONY: dnsmasq
dnsmasq:
	sudo dnsmasq --no-daemon --log-dhcp --conf-file=/etc/dnsmasq.conf

.PHONY: arp-scan
arp-scan:
	sudo arp-scan --interface=enp0s31f6 --localnet
