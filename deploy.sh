#!/bin/bash

source DISTRO

CONTROLLER_IMAGE_FILE=out/raspios/$VERSION/$RELEASE/cluster-controller.img
WORKER_IMAGE_FILE=out/raspios/$VERSION/$RELEASE/cluster-worker.img

NFS_ROOT_SERVER=192.168.178.10

PXE_DIR=/pxe/pi4-cluster

CONTROLLER=dc-a6-32-6b-f0-42
WORKERS=(dc-a6-32-73-13-1a  dc-a6-32-77-71-c9  dc-a6-32-77-7a-8d  dc-a6-32-ba-7a-9c)

# #############################################################################

STAGING_DIR=/tmp/pi4-cluster/staging
STAGING_DIR_TFTP=${STAGING_DIR}/tftp
STAGING_DIR_NFS=${STAGING_DIR}/nfs


# #############################################################################

# https://stackoverflow.com/questions/5947742/how-to-change-the-output-color-of-echo-in-linux
GREEN='\033[1;32m'
BLUE='\033[1;34m'
NC='\033[0m' # No Color

# https://brennan.io/2019/12/04/rpi4b-netboot/

#
#
#
makeStagingDir () {
    mkdir -p ${STAGING_DIR}/{tftp,nfs} > /dev/null 2>&1
}

#
# @param1 - target directory inclusive path
#
prepareDir () {
    [ -e "${1}" ] && {
        rm -rf ${1}/* > /dev/null 2>&1
    } || {
        mkdir -p "${1}"
    }
}

#
# @param1 - image file name inclusive path
#
mountImagefile () {
    # mount image file to mapper device
    local loop_base=$( losetup --partscan --find --show "${1}" )
    # 'boot' partition
    mount ${loop_base}p1 "${STAGING_DIR_TFTP}"
    # 'rootfs' partition
    mount ${loop_base}p2 "${STAGING_DIR_NFS}"
    echo "${loop_base}"
}

#
# @param1 - loop device
#
umountImagefile () {
    umount ${1}p1
    umount ${1}p2
    losetup --detach ${1}
}

#
# @param1 - directory name inclusive path
#
deployBootDir () {

    echo "    rsync 'boot'   partition to '${1}'"

    prepareDir "${1}"

    rsync -ax ${STAGING_DIR_TFTP}/ ${1}

}

#
# @param1 - directory name inclusive path
#
deployRootfsDir () {

    echo "    rsync 'rootfs' partition to '${1}'"

    prepareDir "${1}"

    rsync -ax ${STAGING_DIR_NFS}/ ${1}

    #rsync -ax ${STAGING_DIR_TFTP}/ ${1}/boot

}

#
#
#
deploy2Workers () {

    local cmdlineNFS=$(cat files/boot/cmdline-nfs.template | sed "s/{{nfsroot}}/$NFS_ROOT_SERVER/g" | sed "s/{{type}}/worker/g")
    local fstablineNFS=$(cat files/boot/boot-on-nfs.template | sed "s/{{nfsroot}}/$NFS_ROOT_SERVER/g")

    for mac in ${WORKERS[@]}; do

        local bootDir=${PXE_DIR}/tftp/$mac
        local rootfsDir=${PXE_DIR}/nfs/worker/$mac

        echo
        printf "${BLUE}    [$mac]${NC}\n"

        [ -z "${1}" ] && {1}="none"


        case "${1}" in

            # boot directory on TFTP server
            boot)
                deployBootDir "$bootDir"
                echo "    rsync 'rootfs' partition disabled"
                echo $cmdlineNFS | sed "s/{{mac}}/$mac/g" > ${bootDir}/cmdline.txt
                ;;

            # rootfs directory on NFS server
            rootfs)
                echo "    rsync 'boot'   partition disabled"
                deployRootfsDir "${rootfsDir}"
                sed -i "s/PARTUUID/#PARTUUID/g" ${rootfsDir}/etc/fstab
                echo ${fstablineNFS} | sed "s/{{mac}}/${mac}/g" >> ${rootfsDir}/etc/fstab
                ;;

            # boot and rootfs directory on TFTP/NFS server
            all)
                deployBootDir "$bootDir"
                echo $cmdlineNFS | sed "s/{{mac}}/$mac/g" > ${bootDir}/cmdline.txt
                deployRootfsDir "${rootfsDir}"
                sed -i "s/PARTUUID/#PARTUUID/g" ${rootfsDir}/etc/fstab
                echo ${fstablineNFS} | sed "s/{{mac}}/${mac}/g" >> ${rootfsDir}/etc/fstab
                ;;

            none | "")
                echo "    rsync 'boot'   partition disabled"
                echo "    rsync 'rootfs' partition disabled"
                ;;

        esac


    done
}

#
#
#
deploy2Controller () {

    local cmdlineNFS=$(cat files/boot/cmdline-nfs.template | sed "s/{{nfsroot}}/$NFS_ROOT_SERVER/g" | sed "s/{{type}}/controller/g")
    local fstablineNFS=$(cat files/boot/boot-on-nfs.template | sed "s/{{nfsroot}}/$NFS_ROOT_SERVER/g")

    local bootDir=${PXE_DIR}/tftp/${CONTROLLER}
    local rootfsDir=${PXE_DIR}/nfs/controller/${CONTROLLER}

    echo
    printf "${BLUE}    [${CONTROLLER}]${NC}\n"

    case "${1}" in

        # boot directory on TFTP server
        boot)
            deployBootDir "$bootDir"
            echo "    rsync 'rootfs' partition disabled"
            echo $cmdlineNFS | sed "s/{{mac}}/${CONTROLLER}/g" > ${bootDir}/cmdline.txt
            ;;

        # rootfs directory on NFS server
        rootfs)
            echo "    rsync 'boot'   partition disabled"
            deployRootfsDir "${rootfsDir}"
            sed -i "s/PARTUUID/#PARTUUID/g" ${rootfsDir}/etc/fstab
            echo ${fstablineNFS} | sed "s/{{mac}}/${CONTROLLER}/g" >> ${rootfsDir}/etc/fstab
            ;;

        # boot and rootfs directory on TFTP/NFS server
        all)
            deployBootDir "$bootDir"
            echo $cmdlineNFS | sed "s/{{mac}}/${CONTROLLER}/g" > ${bootDir}/cmdline.txt
            deployRootfsDir "${rootfsDir}"
            sed -i "s/PARTUUID/#PARTUUID/g" ${rootfsDir}/etc/fstab
            echo ${fstablineNFS} | sed "s/{{mac}}/${CONTROLLER}/g" >> ${rootfsDir}/etc/fstab
            ;;

        none)
            echo "    rsync 'boot'   partition disabled"
            echo "    rsync 'rootfs' partition disabled"
            ;;

    esac

}

#
# cluster_worker
#
cluster_workers () {

    printf "${GREEN}==> cluster workers${NC}\n"

    [ "${1}" = "none" ] && {
        echo "--> deployment disabled per option" "'${1}'"
        echo
        return
    }

    echo -n "--> mounting '${WORKER_IMAGE_FILE}' on "
    loop_base=$(mountImagefile "${WORKER_IMAGE_FILE}")
    echo "'${loop_base}'"

    deploy2Workers "${1}"
    echo

    echo "--> unmounting and detach '${loop_base}'"
    umountImagefile "$loop_base"
    echo
}

#
# cluster_controller
#
cluster_controller () {

    echo
    printf "${GREEN}==> cluster controller${NC}\n"

    [ "${1}" = "none" ] && {
        echo "--> deployment disabled per option '${1}'"
        echo
        return
    }

    echo -n "--> mounting '${CONTROLLER_IMAGE_FILE}' on "
    loop_base=$(mountImagefile "${CONTROLLER_IMAGE_FILE}")
    echo "'${loop_base}'"

    deploy2Controller "${1}"
    echo

    echo "--> unmounting and detach '${loop_base}'"
    umountImagefile "$loop_base"
    echo

}

if [ "$EUID" -ne 0 ]
    then echo "Please run as root"
    exit
fi

echo "Deployment to TFTP/NFS server on '${NFS_ROOT_SERVER}'"
makeStagingDir
cluster_controller "all"
cluster_workers "none"
