#!/bin/bash

#
# @param1 string - name of the command line tool
#
isInstalled () {
	which "$1" >/dev/null 2>&1
	echo $?
}

#
#
#
checkPrerequests () {

	local p_nmap="nmap"

	[ $(isInstalled "$p_nmap") -eq 1 ] && {
        	echo "$p_nmap: this program is missing on our system"
        	exit -2
	}
	

}

#
# @param1 string - Classless Inter-Domain Routing e.g. 192.168.0.0/24
#
fillARPCache () {

	nmap -sn "$1" >/dev/null 2>&1
}

#
# @param1 string - the first 3 MAC adress numbers as pattern
#
readRPiIPsfromARPCache () {
	echo $(ip n | grep "$1" |  cut -d " " -f 1)
}


#
# @param1 string - the first 3 MAC adress numbers as pattern
#
readRPiIPsandMACsfromARPCache () {
	echo $(ip n | grep "$1" |  cut -d " " -f 1,5)
}

#
# @param1 array - KV list of 'mac ip'
#
listIPs () {

	IFS=' '
	
	local AR=("$@")
	local ip=""
	local mac=""

	for ((idx=0; idx<${#AR[@]}; ++idx));do

		r=$(( $idx % 2 ))

		[ ${r} -eq 0 ] && {
			ip="${AR[idx]}"
		} || {
			mac="${AR[idx]}"
			echo "$mac $ip"
		}
	    	
	done


}

#
# main
#

#ssh-keygen -f "/home/ufuchs/.ssh/known_hosts" -R "192.168.178.119"

RPI_4_MAC_FIRST3="dc:a6:32"

echo "==> checkPrerequests"
checkPrerequests

echo "==> fillARPCache"
fillARPCache "192.168.178.0/24"

echo "==> sleep 5"
sleep 5

echo "==> readRPiIPsandMACsfromARPCache"
read -a RPI_4_IP <<< $(readRPiIPsandMACsfromARPCache "$RPI_4_MAC_FIRST3")

listIPs "${RPI_4_IP[@]}"










