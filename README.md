[pi4-pxe]
https://williamlam.com/2020/07/two-methods-to-network-boot-raspberry-pi-4.html
|-> https://codestrian.com/index.php/2020/02/14/setting-up-a-pi-cluster-with-netboot/

[pxe]
https://warlord0blog.wordpress.com/2020/02/29/pxe-booting-from-a-container/
https://jacobrsnyder.com/2021/01/20/network-booting-a-raspberry-pi-with-docker-support/


[pxe-server]
https://github.com/ferrarimarco/docker-pxe
https://libs.garden/docker/ferrarimarco/docker-pxe
https://jpetazzo.github.io/2013/12/07/pxe-netboot-docker/
https://a-t-engineering.com/en/pxe-network-boot-using-netboot-xyz-under-docker/

[Beifang]
https://www.linuxserver.io/blog/tag:TFTP#blog_list
https://discourse.pi-hole.net/t/setting-up-a-pxe-boot-server-with-docker-compose-and-boot-kali-linux-or-other-oss/50218
https://blog.haschek.at/2019/build-your-own-datacenter-with-pxe-and-alpine.html
